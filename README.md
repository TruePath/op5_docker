# op5-docker
> This is a bacis example of how you might deploy OP5 via Docker. This is only a guide and several pre-configuration steps must be taken before this will work.

## Build

```
make build
```

## Run

```
make run
```

## Clean

```
make clean
```
